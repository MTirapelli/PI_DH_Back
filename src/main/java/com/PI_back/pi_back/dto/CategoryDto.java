package com.PI_back.pi_back.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
@AllArgsConstructor

public class CategoryDto {
    private String name;

}
